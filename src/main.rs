// Copyright © 2019 Intel Corporation
//
// SPDX-License-Identifier: Apache-2.0
//

use clap::{Arg, ArgAction, ArgGroup, ArgMatches, Command};
use libc::EFD_NONBLOCK;
use log::{warn, LevelFilter};
use option_parser::OptionParser;
use std::env;
use std::os::unix::io::{RawFd};
use std::sync::mpsc::channel;
use std::sync::{Arc, Mutex};
use thiserror::Error;
use vmm::api::ApiAction;
use vmm::config;
use vmm_sys_util::eventfd::EventFd;
use vmm_sys_util::signal::block_signal;

#[derive(Error, Debug)]
enum Error {
    #[error("Failed to create API EventFd: {0}")]
    CreateApiEventFd(#[source] std::io::Error),
    #[error("Failed to create exit EventFd: {0}")]
    CreateExitEventFd(#[source] std::io::Error),
    #[error("Failed to open hypervisor interface (is hypervisor interface available?): {0}")]
    CreateHypervisor(#[source] hypervisor::HypervisorError),
    #[error("Failed to start the VMM thread: {0}")]
    StartVmmThread(#[source] vmm::Error),
    #[error("Error parsing config: {0}")]
    ParsingConfig(vmm::config::Error),
    #[error("Error creating VM: {0:?}")]
    VmCreate(vmm::api::ApiError),
    #[error("Error booting VM: {0:?}")]
    VmBoot(vmm::api::ApiError),
    #[error("Failed to join on VMM thread: {0:?}")]
    ThreadJoin(std::boxed::Box<dyn std::any::Any + std::marker::Send>),
    #[error("VMM thread exited with error: {0}")]
    VmmThread(#[source] vmm::Error),
    #[error("Error parsing --api-socket: {0}")]
    ParsingApiSocket(std::num::ParseIntError),
    #[error("Error creating log file: {0}")]
    LogFileCreation(std::io::Error),
    #[error("Error setting up logger: {0}")]
    LoggerSetup(log::SetLoggerError),
}

struct Logger {
    output: Mutex<Box<dyn std::io::Write + Send>>,
    start: std::time::Instant,
}

impl log::Log for Logger {
    fn enabled(&self, _metadata: &log::Metadata) -> bool {
        true
    }

    fn log(&self, record: &log::Record) {
        if !self.enabled(record.metadata()) {
            return;
        }

        let now = std::time::Instant::now();
        let duration = now.duration_since(self.start);

        if record.file().is_some() && record.line().is_some() {
            write!(
                *(*(self.output.lock().unwrap())),
                "cloud-hypervisor: {:.6?}: <{}> {}:{}:{} -- {}\r\n",
                duration,
                std::thread::current().name().unwrap_or("anonymous"),
                record.level(),
                record.file().unwrap(),
                record.line().unwrap(),
                record.args()
            )
        } else {
            write!(
                *(*(self.output.lock().unwrap())),
                "cloud-hypervisor: {:.6?}: <{}> {}:{} -- {}\r\n",
                duration,
                std::thread::current().name().unwrap_or("anonymous"),
                record.level(),
                record.target(),
                record.args()
            )
        }
        .ok();
    }
    fn flush(&self) {}
}

fn prepare_default_values() -> (String, String, String) {
    (default_vcpus(), default_memory(), default_rng())
}

fn default_vcpus() -> String {
    format!(
        "boot={},max_phys_bits={}",
        config::DEFAULT_VCPUS,
        config::DEFAULT_MAX_PHYS_BITS
    )
}

fn default_memory() -> String {
    format!("size={}M", config::DEFAULT_MEMORY_MB)
}

fn default_rng() -> String {
    format!("src={}", config::DEFAULT_RNG_SOURCE)
}

fn create_app(default_vcpus: String, default_memory: String, default_rng: String) -> Command {
    #[allow(clippy::let_and_return)]
    let app = Command::new("cloud-hypervisor")
        // 'BUILD_VERSION' is set by the build script 'build.rs' at
        // compile time
        .author(env!("CARGO_PKG_AUTHORS"))
        .about("Launch a cloud-hypervisor VMM.")
        .group(ArgGroup::new("vm-config").multiple(true))
        .group(ArgGroup::new("vmm-config").multiple(true))
        .group(ArgGroup::new("logging").multiple(true))
        .arg(
            Arg::new("cpus")
                .long("cpus")
                .help(
                    "boot=<boot_vcpus>,max=<max_vcpus>,\
                    topology=<threads_per_core>:<cores_per_die>:<dies_per_package>:<packages>,\
                    kvm_hyperv=on|off,max_phys_bits=<maximum_number_of_physical_bits>,\
                    affinity=<list_of_vcpus_with_their_associated_cpuset>,\
                    features=<list_of_features_to_enable>",
                )
                .default_value(default_vcpus)
                .group("vm-config"),
        )
        .arg(
            Arg::new("platform")
                .long("platform")
                .help("num_pci_segments=<num_pci_segments>,iommu_segments=<list_of_segments>,serial_number=<dmi_device_serial_number>,uuid=<dmi_device_uuid>,oem_strings=<list_of_strings>")
                .num_args(1)
                .group("vm-config"),
        )
        .arg(
            Arg::new("memory")
                .long("memory")
                .help(
                    "Memory parameters \
                     \"size=<guest_memory_size>,mergeable=on|off,shared=on|off,\
                     hugepages=on|off,hugepage_size=<hugepage_size>,\
                     hotplug_method=acpi|virtio-mem,\
                     hotplug_size=<hotpluggable_memory_size>,\
                     hotplugged_size=<hotplugged_memory_size>,\
                     prefault=on|off,thp=on|off\"",
                )
                .default_value(default_memory)
                .group("vm-config"),
        )
        .arg(
            Arg::new("memory-zone")
                .long("memory-zone")
                .help(
                    "User defined memory zone parameters \
                     \"size=<guest_memory_region_size>,file=<backing_file>,\
                     shared=on|off,\
                     hugepages=on|off,hugepage_size=<hugepage_size>,\
                     host_numa_node=<node_id>,\
                     id=<zone_identifier>,hotplug_size=<hotpluggable_memory_size>,\
                     hotplugged_size=<hotplugged_memory_size>,\
                     prefault=on|off\"",
                )
                .num_args(1..)
                .group("vm-config"),
        )
        .arg(
            Arg::new("firmware")
                .long("firmware")
                .help("Path to firmware that is loaded in an architectural specific way")
                .num_args(1)
                .group("vm-config"),
        )
        .arg(
            Arg::new("kernel")
                .long("kernel")
                .help(
                    "Path to kernel to load. This may be a kernel or firmware that supports a PVH \
                entry point (e.g. vmlinux) or architecture equivalent",
                )
                .num_args(1)
                .group("vm-config"),
        )
        .arg(
            Arg::new("initramfs")
                .long("initramfs")
                .help("Path to initramfs image")
                .num_args(1)
                .group("vm-config"),
        )
        .arg(
            Arg::new("cmdline")
                .long("cmdline")
                .help("Kernel command line")
                .num_args(1)
                .group("vm-config"),
        )
        .arg(
            Arg::new("rng")
                .long("rng")
                .help(
                    "Random number generator parameters \"src=<entropy_source_path>,iommu=on|off\"",
                )
                .default_value(default_rng)
                .group("vm-config"),
        )
        .arg(
            Arg::new("serial")
                .long("serial")
                .help("Control serial port: off|null|pty|tty|file=</path/to/a/file>|socket=</path/to/a/file>")
                .default_value("null")
                .group("vm-config"),
        )
        .arg(
            Arg::new("console")
                .long("console")
                .help(
                    "Control (virtio) console: \"off|null|pty|tty|file=</path/to/a/file>,iommu=on|off\"",
                )
                .default_value("tty")
                .group("vm-config"),
        )
        .arg(
            Arg::new("device")
                .long("device")
                .help(config::DeviceConfig::SYNTAX)
                .num_args(1..)
                .group("vm-config"),
        )
        .arg(
            Arg::new("v")
                .short('v')
                .action(ArgAction::Count)
                .help("Sets the level of debugging output")
                .group("logging"),
        )
        .arg(
            Arg::new("log-file")
                .long("log-file")
                .help("Log file. Standard error is used if not specified")
                .num_args(1)
                .group("logging"),
        )
        .arg(
            Arg::new("api-socket")
                .long("api-socket")
                .help("HTTP API socket (UNIX domain socket): path=</path/to/a/file> or fd=<fd>.")
                .num_args(1)
                .group("vmm-config"),
        );

    #[cfg(target_arch = "x86_64")]
    let app = app.arg(
        Arg::new("debug-console")
            .long("debug-console")
            .help("Debug console: off|pty|tty|file=</path/to/a/file>,iobase=<port in hex>")
            .default_value("off,iobase=0xe9")
            .group("vm-config"),
    );

    app.arg(
        Arg::new("version")
            .short('V')
            .long("version")
            .action(ArgAction::SetTrue)
            .help("Print version")
            .num_args(0),
    )
}

fn start_vmm(cmd_arguments: ArgMatches) -> Result<Option<String>, Error> {
    let log_level = match cmd_arguments.get_count("v") {
        0 => LevelFilter::Warn,
        1 => LevelFilter::Info,
        2 => LevelFilter::Debug,
        _ => LevelFilter::Trace,
    };

    let log_file: Box<dyn std::io::Write + Send> = if let Some(ref file) =
        cmd_arguments.get_one::<String>("log-file")
    {
        Box::new(std::fs::File::create(std::path::Path::new(file)).map_err(Error::LogFileCreation)?)
    } else {
        Box::new(std::io::stderr())
    };

    log::set_boxed_logger(Box::new(Logger {
        output: Mutex::new(log_file),
        start: std::time::Instant::now(),
    }))
    .map(|()| log::set_max_level(log_level))
    .map_err(Error::LoggerSetup)?;

    let (api_socket_path, api_socket_fd) =
        if let Some(socket_config) = cmd_arguments.get_one::<String>("api-socket") {
            let mut parser = OptionParser::new();
            parser.add("path").add("fd");
            parser.parse(socket_config).unwrap_or_default();

            if let Some(fd) = parser.get("fd") {
                (
                    None,
                    Some(fd.parse::<RawFd>().map_err(Error::ParsingApiSocket)?),
                )
            } else if let Some(path) = parser.get("path") {
                (Some(path), None)
            } else {
                (
                    cmd_arguments
                        .get_one::<String>("api-socket")
                        .map(|s| s.to_string()),
                    None,
                )
            }
        } else {
            (None, None)
        };

    let (api_request_sender, api_request_receiver) = channel();
    let api_evt = EventFd::new(EFD_NONBLOCK).map_err(Error::CreateApiEventFd)?;

    let api_request_sender_clone = api_request_sender.clone();

    // Before we start any threads, mask the signals we'll be
    // installing handlers for, to make sure they only ever run on the
    // dedicated signal handling thread we'll start in a bit.
    for sig in &vmm::vm::Vm::HANDLED_SIGNALS {
        if let Err(e) = block_signal(*sig) {
            eprintln!("Error blocking signals: {e}");
        }
    }

    for sig in &vmm::Vmm::HANDLED_SIGNALS {
        if let Err(e) = block_signal(*sig) {
            eprintln!("Error blocking signals: {e}");
        }
    }

    let hypervisor = hypervisor::new().map_err(Error::CreateHypervisor)?;

    let exit_evt = EventFd::new(EFD_NONBLOCK).map_err(Error::CreateExitEventFd)?;

    let vmm_thread_handle = vmm::start_vmm_thread(
        vmm::VmmVersionInfo::new(env!("BUILD_VERSION"), env!("CARGO_PKG_VERSION")),
        &api_socket_path,
        api_socket_fd,
        api_evt.try_clone().unwrap(),
        api_request_sender_clone,
        api_request_receiver,
        exit_evt.try_clone().unwrap(),
        hypervisor,
    )
    .map_err(Error::StartVmmThread)?;

    let r: Result<(), Error> = (|| {
        let payload_present =
            cmd_arguments.contains_id("kernel") || cmd_arguments.contains_id("firmware");

        if payload_present {
            let vm_params = config::VmParams::from_arg_matches(&cmd_arguments);
            let vm_config = config::VmConfig::parse(vm_params).map_err(Error::ParsingConfig)?;

            // Create and boot the VM based off the VM config we just built.
            let sender = api_request_sender.clone();
            vmm::api::VmCreate
                .send(
                    api_evt.try_clone().unwrap(),
                    api_request_sender,
                    Arc::new(Mutex::new(vm_config)),
                )
                .map_err(Error::VmCreate)?;
            vmm::api::VmBoot
                .send(api_evt.try_clone().unwrap(), sender, ())
                .map_err(Error::VmBoot)?;
        }

        Ok(())
    })();

    if r.is_err() {
        if let Err(e) = exit_evt.write(1) {
            warn!("writing to exit EventFd: {e}");
        }
    }

    vmm_thread_handle
        .thread_handle
        .join()
        .map_err(Error::ThreadJoin)?
        .map_err(Error::VmmThread)?;

    r.map(|_| api_socket_path)
}

fn main() {
    // Ensure all created files (.e.g sockets) are only accessible by this user
    // SAFETY: trivially safe
    let _ = unsafe { libc::umask(0o077) };

    let (default_vcpus, default_memory, default_rng) = prepare_default_values();
    let cmd_arguments = create_app(default_vcpus, default_memory, default_rng).get_matches();

    if cmd_arguments.get_flag("version") {
        println!("{} {}", env!("CARGO_BIN_NAME"), env!("BUILD_VERSION"));

        if cmd_arguments.get_count("v") != 0 {
            println!("Enabled features: {:?}", vmm::feature_list());
        }

        return;
    }

    let exit_code = match start_vmm(cmd_arguments) {
        Ok(path) => {
            path.map(|s| std::fs::remove_file(s).ok());
            0
        }
        Err(e) => {
            eprintln!("{e}");
            1
        }
    };

    std::process::exit(exit_code);
}
